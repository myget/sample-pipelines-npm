# MyGet - npm pipelines sample

This project contains an example of building, packaging and publishing an npm library to [MyGet](http://www.myget.org) using Bitbucket Pipelines. Once published to MyGet, the package can be used in other projects using the [NuGet](http://ww.nuget.org) package manager.

The pipelines configuration:

* Uses the node 5.10-slim Docker image
* Runs the following commands:
	* `npm install`
	* `npm test` (optional, disabled by default)
	* `npm pack`
	* Publishes the package to a [MyGet](http://www.myget.org) feed

Before being able to invoke the Bitbucket pipeline, there are a couple of environment variables to be configured:

* `MYGET_NPM_REGISTRY_URL`: The full URL of the NuGet feed on MyGet ***without* https://**
* `MYGET_NPM_USERNAME`: MyGet username
* `MYGET_NPM_APIKEY`: yGet API key (***base 64 encoded***)
